FROM ubuntu:latest as base


RUN apt-get -y update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install ca-certificates curl --no-install-recommends && \
    curl -H 'Cache-Control: no-cache' \
        https://raw.githubusercontent.com/nimbix/jarvice-desktop/master/install-nimbix.sh \
        | bash

# Dependencies to build llama-cpp
RUN apt install -y --no-install-recommends \
  python3.11 python3.11-dev python3.11-distutils python3.11-venv \
  python3-pip \
  libopenblas-dev\
  ninja-build\
  build-essential\
  pkg-config\
  wget\
  curl

RUN wget https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py

# Install poetry
RUN pip install pipx
RUN python3 -m pipx ensurepath
RUN pipx install poetry
ENV PATH="/root/.local/bin:$PATH"
ENV PATH=".venv/bin/:$PATH"


# https://python-poetry.org/docs/configuration/#virtualenvsin-project
ENV POETRY_VIRTUALENVS_IN_PROJECT=true

FROM base as dependencies

WORKDIR /opt
RUN wget -O - https://api.github.com/repos/imartinez/privateGPT/tarball | tar xz

RUN python3 -m pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu
#RUN poetry install --with local
#RUN poetry install --with ui

COPY /etc/NAE/AppDef.json /etc/NAE/AppDef.json
RUN curl --fail -X POST -d @/etc/NAE/AppDef.json https://cloud.nimbix.net/api/jarvice/validate