#!/usr/bin/env bash

set -eufx -o pipefail
export PATH=$PATH:/bin:/usr/bin
export CURRENTLY_BUILDING_DOCKER_IMAGE=1 \
    container=docker

export DEBIAN_FRONTEND=noninteractive   \
    LANGUAGE=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8
apt-get -yqq update \
 && apt-get -yqq upgrade \
 && apt-get -yqq install --no-install-recommends \
        build-essential \
        ca-certificates \
        curl \
        file \
        g++ \
        gcc \
        gfortran \
        git \
        gnupg2 \
        iproute2 \
        locales \
        make \
        mercurial \
        subversion \
        python3 \
        python3-pip \
        python3-setuptools \
        unzip \
        rsync \
        zstd
# && locale-gen en_US.UTF-8

export SPACK_ROOT=/opt/spack

( mkdir -p $SPACK_ROOT && cd $SPACK_ROOT && \
    git init --quiet && git remote add origin https://github.com/dguibert/spack.git && git fetch --depth=1 origin develop && git checkout --detach FETCH_HEAD && \
    mkdir -p $SPACK_ROOT/opt/spack ) || true

( mkdir -p /opt/compbiomed-spack && cd /opt/compbiomed-spack && \
    git init --quiet && git remote add origin https://github.com/dguibert/compbiomed-spack && git fetch --depth=1 origin 4fc4040352587abc38917b1dad398a2d79c6ec4c && git checkout --detach 4fc4040352587abc38917b1dad398a2d79c6ec4c ) || true

mkdir -p /opt/spack-configs && \
    rsync -aP spack-configs/ /opt/spack-configs/
#cd /opt/spack-configs && \
#    git init --quiet && git remote add origin https://castle.frec.bull.fr:24443/bguibertd/spack-configs \
#&& git fetch --depth=1 origin d3f67bb775ef67a6ac41f1b6e973cb38031209cf && git checkout --detach d3f67bb775ef67a6ac41f1b6e973cb38031209cf

set +x
. $SPACK_ROOT/share/spack/setup-env.sh
. $SPACK_ROOT/share/spack/spack-completion.bash
set -x
# Creates the package cache
spack bootstrap now
spack bootstrap status --optional
spack repo add /opt/spack-configs/repos/bench || true
spack repo add /opt/compbiomed-spack || true
spack spec hdf5+mpi

# What we want to install and how we want to install it
# is specified in a manifest file (spack.yaml)
mkdir -p /opt/spack-environment
cp spack.yaml /opt/spack-environment/spack.yaml

# Install the software, remove unnecessary deps
cd /opt/spack-environment
spack -e . concretize -f
spack -e . install --fail-fast -j $(nproc)

# Modifications to the environment that are necessary to run
spack env activate --sh -d . > activate.sh
spack env activate --sh -d .

spack -e . buildcache push --base-image ubuntu:22.04 --tag latest_build_x86_64_v3 container-registry
