#!/usr/bin/env bash
#
set -eux -o pipefail

command -v nix
test ! -e /tmp/ubuntu-22.04 && sudo nix run nixpkgs#debootstrap -- --include=dbus-broker,systemd-container --components=main,universe jammy /tmp/ubuntu-22.04 http://archive.ubuntu.com/ubuntu/
mkdir -p /tmp/spack-environment
mkdir -p /tmp/opt
sudo  SYSTEMD_LOG_LEVEL=debug systemd-nspawn -x -D /tmp/ubuntu-22.04 --resolv-conf=replace-host --tmpfs=/run/lock --bind=/tmp/opt:/opt --bind=/tmp/spack-environment:/opt/spack-environment --bind-ro $PWD:/data /bin/bash -c "(cd /data && /bin/bash ./build.sh)"
