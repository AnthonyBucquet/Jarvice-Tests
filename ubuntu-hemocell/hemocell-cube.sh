#!/bin/bash

set -eufx -o pipefail

command -v cube
command -v mpirun
# Source the JARVICE job environment variables
echo "Sourcing JARVICE environment..."
[[ -r /etc/JARVICE/jobenv.sh ]] && source /etc/JARVICE/jobenv.sh
[[ -r /etc/JARVICE/jobinfo.sh ]] && source /etc/JARVICE/jobinfo.sh

CORES=$(cat /etc/JARVICE/cores | wc -l )
NBNODES=$(cat /etc/JARVICE/nodes | wc -l)

if [[ "$NBNODES" -gt 1 ]]; then
  MPIHOSTS="/etc/JARVICE/cores"
  NBPROCPERNODE=$((CORES/NBNODES))
  echo "MPI environment: "
  echo "  - mpi_hosts list file: $MPIHOSTS"
  echo "  - number of process per nodes: $NBPROCPERNODE"
  echo "  - cores: $CORES"
else
  echo "MPI environment: "
  echo "  - cores: $CORES"
fi
# Enter case directory
echo "Entering case folder ${CASE_FOLDER-/data/inputs-hemocell-cube} ..."
cd "${CASE_FOLDER-/data/inputs-hemocell-cube}"
export HDF5_USE_FILE_LOCKING=FALSE
time mpirun -np $CORES --allow-run-as-root --hostfile /etc/JARVICE/cores -x PATH -x LD_LIBRARY_PATH -x HDF5_USE_FILE_LOCKING cube config.xml |& tee log-$CORES-$$

test -e batchPostProcess.sh && ./batchPostProcess.sh
